package com.projectadahelp.project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonLogin = findViewById(R.id.login_button) as Button;
        val buttonCreateAccount = findViewById(R.id.create_account) as Button;

        buttonLogin.setOnClickListener {
            openDashboard()
        }

        buttonCreateAccount.setOnClickListener {
            createAccount()
        }

    }

    private fun openDashboard(){
        val intent = Intent(this, Dashboard::class.java)
        startActivity(intent)
    }

    private fun createAccount(){
        val intent = Intent(this, CreateAccount::class.java)
        startActivity(intent)
    }
}