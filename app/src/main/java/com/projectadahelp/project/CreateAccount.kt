package com.projectadahelp.project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class CreateAccount : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        val buttonRegister = findViewById(R.id.buttonRegister) as Button;

        buttonRegister.setOnClickListener {
            openDashboard()
        }
    }

    private fun openDashboard(){
        val intent = Intent(this, Dashboard::class.java)
        startActivity(intent)
    }
}